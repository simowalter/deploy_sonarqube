#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m  $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

log_info "*****************************************************************"
log_info "********* Script Deployment of a SonarQube with Docker "*********
log_info "*****************************************************************"

mkdir -p deploy_sonarqube && cd deploy_sonarqube

log_info "**** I - Configuration of hardware requirements for SonarQube. ****"
sudo tee -a /etc/sysctl.conf <<EOF
# conf necessary for the use of elasticsearch and sonarqube 
fs.file-max=131072
vm.max_map_count=524288
EOF

log_info "------------------> Configuration of the number of simulataneous opened files and processes."
ulimit -n 131072
ulimit -u 8192

log_info "------------------> Reload of systctl ."
sudo sysctl --system

log_info "***************** II -  Deployment ***************** " 

log_info " -------------> Generate the docker-compose (Sonar + postgres database)."

cat > docker-compose.yml <<-EOF
version: "3"

services:
  sonarqube:
    image: sonarqube:lts-community
    container_name: sonarqube
    restart: always
    depends_on:
      - database_sonar
    environment:
      SONAR_JDBC_URL: jdbc:postgresql://database_sonar:5432/sonar
      SONAR_JDBC_USERNAME: sonar
      SONAR_JDBC_PASSWORD: sonar_walter_insights
      https_proxy: http://193.56.47.8:8080
      http_proxy: http://193.56.47.8:8080
      ftp_proxy: http://193.56.47.8:8080
      HTTP_PROXY: http://193.56.47.8:8080
      FTP_PROXY: http://193.56.47.8:8080
      HTTPS_PROXY: http://193.56.47.8:8080
      NO_PROXY: gitlab-server.bs.fr.mywalter.net,localhost,127.*
      no_proxy: gitlab-server.bs.fr.mywalter.net,localhost,127.*
      JAVA_OPTS: "-Djavax.net.ssl.trustStore=/opt/java/openjdk/lib/security/cacerts  -Djavax.net.ssl.trustStorePassword=changeit"
    volumes:
      - sonarqube_data:/opt/sonarqube/data
      - sonarqube_extensions:/opt/sonarqube/extensions
      - sonarqube_logs:/opt/sonarqube/logs
      - ./conf_sonarqube:/opt/sonarqube/conf
    command: -Dhttp.proxyHost=193.56.47.8 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=193.56.47.8 -Dhttps.proxyPort=8080 -Dhttp.nonProxyHosts=gitlab-server.bs.fr.mywalter.net|localhost|127.*|[::1]
    ports:
      - "9091:9000"
    networks:
      - sonar_network
  database_sonar:
    image: postgres:15.2-alpine
    container_name: postgres_sonarqube
    restart: always
    environment:
      POSTGRES_USER: sonar
      POSTGRES_PASSWORD: sonar_walter_insights
    volumes:
      - postgres_sonarqube:/var/lib/postgresql
    networks:
      - sonar_network
volumes:
  sonarqube_data:
  sonarqube_extensions:
  sonarqube_logs:
  postgres_sonarqube:
networks:
  sonar_network:
    external: false
    ipam:
      config:
        - subnet: 172.18.0.0/16
EOF

log_info " -------------> 🚀🚀 running the docker-compose 🚀🚀."
docker-compose up -d



log_info " -------------> 🚀🚀 running the docker-compose 🚀🚀."
docker exec -it -u root sonarqube sed -i 's/#https.proxyHost=/https.proxyHost=193.56.47.8/' conf/sonar.properties
docker exec -it -u root sonarqube sed -i 's/#http.proxyHost=/http.proxyHost=193.56.47.8/' conf/sonar.properties
docker exec -it -u root sonarqube sed -i 's/#https.proxyPort=/https.proxyPort=8080/' conf/sonar.properties
docker exec -it -u root sonarqube sed -i 's/#http.proxyPort=/https.proxyPort=8080/' conf/sonar.properties
docker exec -it -u root sonarqube sed -i 's/#http.nonProxyHosts=/http.nonProxyHosts=gitlab-server.bs.fr.mywalter.net|localhost|127.*|[::1]/' conf/sonar.properties
docker exec -it -u root sonarqube sed -i 's/#sonar.web.javaAdditionalOpts=/sonar.web.javaAdditionalOpts=-Dhttps.proxyHost=193.56.47.8 -Dhttps.proxyPort=8080 -Dhttp.proxyHost=193.56.47.8 -Dhttp.proxyPort=8080 -Dhttp.nonProxyHosts=gitlab-server.bs.fr.mywalter.net|localhost|127.*|[::1]/' conf/sonar.properties
docker exec -it -u root sonarqube sed -i 's/#http.nonProxyHosts=/http.nonProxyHosts=gitlab-server.bs.fr.mywalter.net/' conf/sonar.properties 
docker restart sonarqube

docker cp ca.crt sonarqube:/opt/sonarqube/

keytool -import -trustcacerts -alias gitlab -file /opt/sonarqube/ca.crt -keystore /opt/sonarqube/truststore.jks

log_success " SonarQube successfully deployed "

log_info " -------------> Please login into the User interface of SonarQube at http://localhost:9091 ."

